//
//  WLHomeMenuVC.swift
//  Curling Companion
//
//  Created by Sam Wilskey on 12/2/17.
//  Copyright © 2017 Sam Wilskey. All rights reserved.
//

import UIKit

class WLHomeMenuVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func navigationAction(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            print("Shot Measure")
            
            break
        case 1:
            let rulesVC = WLRulesAndTermsVC()
            present(rulesVC, animated: true, completion: nil)
            break
        case 2:
            let calendarVC = WLCalendarVC()
            present(calendarVC, animated: true, completion: nil)
            break
        case 3:
            let scoreKeepingVC = WLScoreKeepingVC()
            present(scoreKeepingVC, animated: true, completion: nil)
            break
        case 4:
            let settingsVC = WLSettingsVC()
            present(settingsVC, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
